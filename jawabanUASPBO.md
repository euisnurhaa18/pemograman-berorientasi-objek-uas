# NO.1
Use case adalah deskripsi dari interaksi antara pengguna dan sistem atau produk dalam konteks tertentu. Ini mencakup aktor (entitas yang berinteraksi), aksi (tindakan yang dilakukan oleh aktor), dan hasil yang diharapkan dari interaksi tersebut. Use case membantu memahami bagaimana pengguna akan menggunakan sistem atau produk dan digunakan dalam analisis kebutuhan, perancangan, pengembangan, dan pengujian.

[use case](USE_CASE_KESELURUHAN.pdf) 

# No.2
Class Diagram dari keseluruhan Use Case produk digital
[class diagram](Untitled_Diagram-Page-15.drawio__3_.png)

# NO.3
contoh kode dalam bahasa PHP yang menunjukkan sebuah halaman login untuk admin. Untuk menjelaskan penerapan setiap poin dari SOLID Design Principle, berikut adalah penjelasan untuk masing-masing prinsip:
1. Single Responsibility Principle (SRP):
   Dalam kode tersebut, setiap file memiliki tanggung jawab yang spesifik. File `header.php` berisi kode untuk menampilkan header, file `footer.php` berisi kode untuk menampilkan footer, dan file `koneksi.php` bertanggung jawab untuk mengatur koneksi ke database. Hal ini menunjukkan penerapan SRP dengan memisahkan tanggung jawab yang berbeda ke dalam file yang terpisah.

2. Open/Closed Principle (OCP):
   Pada kode tersebut, tidak ada contoh yang jelas tentang penerapan OCP. Prinsip ini lebih relevan dalam konteks desain yang lebih kompleks dan tidak selalu harus diterapkan dalam setiap bagian kode.

3. Liskov Substitution Principle (LSP):
   LSP tidak terlihat secara langsung dalam kode di atas karena tidak ada kelas turunan yang terlibat dalam contoh tersebut.

4. Interface Segregation Principle (ISP):
   ISP tidak terlihat secara langsung dalam kode di atas karena tidak ada penggunaan antarmuka yang jelas.

5. Dependency Inversion Principle (DIP):
   Pada kode tersebut, terdapat contoh penggunaan DIP melalui file `koneksi.php`. File ini bertanggung jawab untuk mengatur koneksi ke database. Dengan memisahkan logika pengaturan koneksi ke dalam file terpisah, penerapan DIP tercapai dengan mengisolasi kode yang bergantung pada detail implementasi seperti koneksi database.

Penerapan SOLID Design Principle tidak selalu jelas dalam setiap bagian kode. Dalam keseluruhan webservice ini mengikuti prinsip-prinsip SOLID Design Principles dengan memisahkan tanggung jawab, memungkinkan perluasan, memisahkan dependensi, dan fokus pada abstraksi yang tepat. Hal ini membuat webservice menjadi lebih terstruktur, mudah dipahami, dan memungkinkan untuk mengelola perubahan dengan lebih baik.

# NO.4
menunjukkan dan menjelaskan Design Pattern yang dipilih
1. MVC (Model-View-Controller):
Terdapat beberapa file yang di-require, seperti "view/header.php" dan "view/footer.php". Jika kita mengasumsikan bahwa file-file ini berisi tampilan (view), maka kita dapat mengasumsikan adanya pemisahan antara model, view, dan controller. Namun, dari potongan kode yang diberikan, tidak dapat dilihat dengan jelas bagaimana model dan controller diimplementasikan secara langsung. 
2. Front Controller:
Kode memproses permintaan POST pada form "pemesanan" yang dikirim ke "pemesanan" melalui aksi "action" di tag form. Oleh karena itu, kita dapat mengasumsikan adanya pemisahan antara logika pemrosesan permintaan dan tampilan pada aksi "pemesanan". Namun, potongan kode yang diberikan tidak memberikan cukup informasi untuk memastikan penggunaan desain pola Front Controller.
[ss pemesanan](Screenshot__84_.png)

# NO.5
untuk KONEKTIVITAS ke DATABASE
1. buka aplikasi xampp lalu klik start pada my sql dan apache, lalu klik tulisan "admin" pada sisi kanan my sql
[xampp](Screenshot__81_.png)
2. membuat data base, berikan nama data base dengan nama sisfohotel kemudian import dan masuken file sisfohotel dengan otomatis tabel tabel akan muncul, seperti di bawah ini.
[phpmyadmin](Screenshot__82_.png)
3. koneksi database di WEBSERVICE
[ss](Screenshot__83_.png)


# NO.6
WEBSERVICE DAN CRUD
- Create (Membuat data baru):
Menerima input dari formulir reservasi (seperti tanggal check-in, tanggal check-out, tipe kamar, harga).
Memproses input tersebut, melakukan validasi data, dan menyimpannya ke dalam database.
[reservasi](Screenshot__88_.png)

- Read (Mengambil data):
Menjalankan query untuk mengambil data kamar dari database.
Menampilkan data kamar ke dalam elemen HTML, seperti menampilkan pilihan tipe kamar pada elemen <select>.
[mengambil data](Screenshot__89_.png)

- Update (Mengubah data):
Menerima input dari formulir untuk mengubah data kamar (seperti harga kamar).
Memproses input tersebut, melakukan validasi data, dan memperbarui data yang sesuai di dalam database.
[mengubah data](Screenshot__90_.png)

- Delete (Menghapus data):
Menerima input (misalnya ID kamar) untuk menghapus data kamar.
Menjalankan query untuk menghapus data yang sesuai dari database.
[hapus data](Screenshot__91_.png)

# NO.7
menunjukkan dan menjelaskan Graphical User Interface dari produk digital
<script type="text/javascript">
  function getData() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api.example.com/data", true);

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var response = JSON.parse(xhr.responseText);
        // Lakukan sesuatu dengan data yang diterima
        console.log(response);
      }
    };

    xhr.send();
  }
</script>

menggunakan objek XMLHttpRequest untuk membuat permintaan GET ke https://api.example.com/data. Ketika respons diterima, kita mem-parse data JSON yang diterima dan dapat melakukan tindakan sesuai dengan kebutuhan. Misalnya, dalam contoh ini, kami hanya mencetak respons ke konsol. kita dapat memanggil fungsi getData() dari elemen HTML seperti tombol atau tautan untuk menginisiasi permintaan HTTP. 
# NO.8
menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
Berikut adalah contoh yang menggambarkan bagaimana Anda dapat melakukan permintaan HTTP POST ke endpoint "user/pemesanan" menggunakan data formulir yang diisi oleh pengguna:
    event.preventDefault(); // Mencegah pengiriman formulir secara default

    // Mengambil nilai-nilai dari elemen formulir
    var checkInDate = document.querySelector('input[name="cekin"]').value;
    var checkOutDate = document.querySelector('input[name="cekout"]').value;
    var roomType = document.querySelector('select[name="tipe"]').value;
    var roomPrice = document.querySelector('input[name="harga"]').value;
    var roomTypeText = document.querySelector('input[name="tipex"]').value;

    // Data formulir yang akan dikirim sebagai payload
    var formData = {
      checkIn: checkInDate,
      checkOut: checkOutDate,
      tipe: roomType,
      harga: roomPrice,
      tipex: roomTypeText
    };

    // Konfigurasi permintaan
    var requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    };

    // Melakukan permintaan HTTP POST
    fetch('user/pemesanan', requestOptions)
      .then(function(response) {
        if (response.ok) {
          // Permintaan berhasil
          console.log('Permintaan berhasil');
          // Lakukan tindakan sesuai kebutuhan
        } else {
          // Permintaan gagal
          console.log('Permintaan gagal');
          // Lakukan tindakan sesuai kebutuhan
        }
      })
      .catch(function(error) {
        console.log('Terjadi kesalahan:', error);
        // Lakukan tindakan sesuai kebutuhan
      });
  }
</script>
kita menambahkan fungsi submitForm() yang akan dipanggil saat formulir dikirim. Fungsi ini mengambil nilai-nilai dari elemen-elemen formulir, membuat payload data dalam format JSON, dan melakukan permintaan HTTP POST ke endpoint "user/pemesanan" menggunakan metode fetch(). juga dapat menambahkan logika tambahan di dalam blok .then() untuk menangani respons yang diterima dari server, seperti menampilkan pesan sukses atau melakukan tindakan sesuai kebutuhan.

Untuk menggunakan fungsi submitForm(), dapat menambahkan atribut onsubmit ke elemen <form> dengan nilai return submitForm(event) untuk memastikan bahwa fungsi ini akan dipanggil saat formulir dikirim. 
<form method="post" action="user/pemesanan" name="opsi" onsubmit="return submitForm(event)">
Pastikan untuk menggantikan URL endpoint dengan yang sesuai untuk permintaan. Selain itu, pastikan bahwa situs web di-host di tempat yang diizinkan untuk melakukan permintaan HTTP ke URL target menggunakan kebijakan keamanan CORS yang tepat.

# NO.9
https://youtu.be/mFnsjTKhmXA 
# NO.10
Mendemonstrasikan penggunaan Machine Learning
